const FS   = require('fs');
const PATH = require('path');

function copyFile(fromFile, toFile) {
    FS.open(fromFile, 'r', (err, fd) => {
        if (err) {console.error(err); process.exit(1); }
        FS.fstat(fd, (err, info) => {
            let buf = Buffer.alloc(info.size);
            FS.read(fd, buf, 0, buf.length, null, (err, byteCount, data) => {
                console.log('read', byteCount, 'bytes from', fromFile);
                let txt = data.toString().toUpperCase();
                FS.close(fd, (err) => {
                    FS.open(toFile, 'w', (err, fd) => {
                        FS.write(fd, txt, (err, byteCount) => {
                            FS.close(fd, (err) => {
                                console.log('written', byteCount, 'bytes to', toFile);
                            });
                        })
                    });
                });
            });
        });
    });
}

const filename = process.argv[2] || PATH.basename(__filename);
copyFile(filename, 'x-' + PATH.basename(filename, '.js') + '.txt');

