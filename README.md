JavaScript, 3 days
====
Welcome to this course.
The syllabus can be found at
[javascript/javascript](https://www.ribomation.se/javascript/javascript.html)

Here you can find
* Installation instructions
* Solutions to the exercises

Installation Instructions
====
In order to do the programming exercises of the course, 
you need to have the following programs/tools installed:

* [Node JS](https://nodejs.org/en/download/)
* [MS Visual Code](https://code.visualstudio.com/Download) or [JetBrains WebStorm (_30 days trial_)](https://www.jetbrains.com/webstorm/download)
* [Google Chrome](https://www.google.se/chrome/browser/desktop/) or [Mozilla Firefox](https://www.mozilla.org/sv-SE/firefox/new/)
* [GIT Client](https://git-scm.com/downloads)

Course GIT Repo
====
Get the course repo initially by a `git clone` operation

    git clone https://gitlab.com/ribomation-courses/javascript/javascript.git
    cd javascript

Get any updates by a `git pull` operation

    git pull

It's recommended that you keep the git repo and your solutions
separated. Create a dedicated directory for this course and
a sub-directory for each chapter.

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

