'use strict';
const N = 100;
for (let k=1; k<=N; ++k) {
	let out = '';
	if (k % 3 === 0) out += 'fizz';
	if (k % 5 === 0) out += 'buzz';
	console.log(out || k);
}
