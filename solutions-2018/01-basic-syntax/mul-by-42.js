const N = 100;
const A = 42;
const S = 10;
for (let k = 1; k <= N; ++k) {
    let prod = k * A;
    console.log(`${k} --> ${prod} ${prod % S === 0 ? ' *****' : ''}`);
}
