'use strict';
const N = 100;
for (let k=1; k<=N; ++k) {	
	console.log((k % 3 ? '' : 'fizz') + (k % 5 ? '' : 'buzz') || k);
}
