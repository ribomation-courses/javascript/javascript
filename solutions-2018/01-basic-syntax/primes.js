let N       = 100;
let numbers = [];
for (let k = 0; k <= N; ++k) numbers[k] = true;

for (let p = 2; p < numbers.length; ++p) {
    while (numbers[p] === false) ++p;
    for (let k = 2*p; k < numbers.length; k += p) numbers[k] = false;
}

let primes = [];
for (let p = 2, k=0; p < numbers.length; ++p) 
	if (numbers[p]) primes[k++] = p;
for (let p of primes) console.log(p);
