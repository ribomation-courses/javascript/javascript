
function sum(n) {
	n = BigInt(n);
    return n * (n + 1n) / 2n;
}

function prod(n) {
	n = BigInt(n);
	let result = 1n;
    for (let k=2n; k<=n; ++k) result *= k;
	return result;
}

function fib(n) {
	n = BigInt(n);
    let f2 = 0n;
	let f1 = 1n;
	let result;
	for (let k = 1n; k<n; ++k) {
		result = f2 + f1;
		f2 = f1;
		f1 = result;
	}
	return result;
}

function table(n) {
    for (let k=1; k<=n; ++k)
		console.log('%o\t%o\t%o\t%o', k, sum(k), fib(k), prod(k));    
}

table(42);
