const FS = require('fs');

JSON.parse(FS.readFileSync('./persons.json').toString())
    .filter(p => p.gender === 'Female')
    .filter(p => 20 <= p.age && p.age <= 30)
    .forEach(p =>
        console.log(`${p.firstName} ${p.lastName}, email: ${p.email}, age: ${p.age}`))
;
