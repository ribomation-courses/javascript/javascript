function complete(ev) {
    let item = ev.target.parentNode.parentNode;
    let when = item.querySelector('.when');
    let expiry = when.querySelector('.date');
    let done = document.createElement('span');

    expiry.classList.add('completed');
    done.classList.add('done');
    when.appendChild(done);
    ev.target.setAttribute('disabled', true);
}

function edit(ev) {
    let what = document.querySelector('#what');
    let when = document.querySelector('#when');
    let item = ev.target.parentNode.parentNode;
    what.value = item.querySelector('.what').textContent.trim();
    when.value = item.querySelector('.when').textContent.trim();
}

function remove(ev) {
    let item = ev.target.parentNode.parentNode;
    document.querySelector('#items').removeChild(item);

    let when = item.querySelector('.when .date').textContent;
    let what = item.querySelector('.what').textContent;
    removeTodo({when: when, what: what});
}

function clearFields(ev) {
    document.querySelector('#what').value = '';
    document.querySelector('#when').value = new Date().toLocaleDateString();
}

function addActions(buttons) {
    for (let k = 0; k < buttons.length; ++k) {
        let b = buttons[k];
        switch (b.innerText) {
            case 'Complete':
                b.addEventListener('click', complete);
                break;
            case 'Edit':
                b.addEventListener('click', edit);
                break;
            case 'Remove':
                b.addEventListener('click', remove);
                break;
        }
    }
}

function appendTodo(when, what) {
    let html = `
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when "><span class="date">${when}</span></p>
        <p class="what">${what}</p>
    `;

    let todo = document.createElement('li');
    todo.className = 'list-group-item';
    todo.innerHTML = html;
    addActions(todo.querySelectorAll('button'));

    document.querySelector('#items').appendChild(todo);
}

const TODO_KEY = 'todos';

function storeTodo(todo) {
    let todos = JSON.parse(localStorage.getItem(TODO_KEY)) || [];
    todos.push(todo);
    localStorage.setItem(TODO_KEY, JSON.stringify(todos));
}

function loadTodos() {
    let todos = JSON.parse(localStorage.getItem(TODO_KEY)) || [];
    todos.forEach(todo => appendTodo(todo.when.substr(0, 10), todo.what));
}

function removeTodo(todo) {
    let todos = JSON.parse(localStorage.getItem(TODO_KEY)) || [];
    let idx = todos.findIndex(t => t.what === todo.what);
    if (idx >= 0) {
        todos.splice(idx, 1);
        localStorage.setItem(TODO_KEY, JSON.stringify(todos));
    }
}

function clearTodos() {
    localStorage.removeItem(TODO_KEY);
}

function saveTodo(ev) {
    ev.preventDefault();
    let when = document.querySelector('#when').value;
    let what = document.querySelector('#what').value;
    if (!what || what.trim().length === 0) return;

    clearFields();
    appendTodo(when, what);
    storeTodo({
        when: new Date(when),
        what: what
    });
}

function removeAllTodos(ev) {
    document.querySelector('#items').innerHTML = '';
    clearTodos();
}

function clearSearchField(ev) {
    let phrase = document.querySelector('#phrase');
    phrase.value = '';
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        items[k].style.display = 'initial';
    }
}

function interactiveSearch(ev) {
    let phrase = document.querySelector('#phrase');
    let text = phrase.value;
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        let todo = items[k];
        if (text && text.trim().length > 0) {
            let what = todo.querySelector('.what').textContent;
            todo.style.display = (what.indexOf(text) === -1) ? 'none' : 'initial';
        } else {
            todo.style.display = 'initial';
        }
    }
}

document.querySelector('#save').addEventListener('click', saveTodo);
document.querySelector('#remove-all').addEventListener('click', removeAllTodos);
document.querySelector('#clear').addEventListener('click', clearFields);
document.querySelector('#phrase-clear').addEventListener('click', clearSearchField);
document.querySelector('#phrase').addEventListener('keyup', interactiveSearch);

let items = document.querySelectorAll('#items li');
for (let k = 0; k < items.length; ++k) {
    addActions(items[k].querySelectorAll('button'));
}
window.addEventListener('load', loadTodos);
clearFields();

