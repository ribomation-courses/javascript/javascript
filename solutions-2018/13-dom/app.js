let interval = 1000;


setTimeout(() => {
	console.info('***', 'overdue');
	
    let deadline = new Date('2018-05-26').getTime();
    let todos = document.querySelectorAll('#items li');
    for (let k = 0; k < todos.length; ++k) {
        let todo = todos[k];
        let when = todo.querySelector('.when');
        let expiry = when.querySelector('.date');
        if (expiry.classList.contains('completed')) continue;

        if (new Date(expiry.innerText).getTime() < deadline) {
            when.classList.add('overdue');
        }
    }
}, interval);


setTimeout(() => {
	console.info('***', 'done');
	
    let todos = document.querySelectorAll('#items li');
    for (let k = 0; k < todos.length; ++k) {
        let todo = todos[k];
        let when = todo.querySelector('.when');
        let expiry = when.querySelector('.date');
        if (expiry.classList.contains('completed') || when.classList.contains('overdue')) continue;

        expiry.classList.add('completed');
        let done = document.createElement('span');
        done.classList.add('done');
        when.appendChild(done);

        break;
    }
}, interval += interval);


setTimeout(() => {
	console.info('***', 'add item');
	
    let when = '2018-06-06';
    let what = 'Complete this darn exercise';
    let html = `
    <li class="list-group-item">
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when "><span class="date">${when}</span></p>
        <p class="what">${what}</p>
    </li>
    `;

    let todo  = document.createElement('li');
    let todos = document.querySelector('#items');
    todos.appendChild(todo);
    todo.outerHTML = html;
}, interval += interval);


setTimeout(() => {
	const what = document.querySelector('#what');
	console.warn('***', 'what:', what.value);	
}, interval += interval);


setTimeout(() => {
	const todos = document.querySelectorAll('#items li');
	const obj = {};
	for (let k = 0; k < todos.length; ++k) {
		const todo = todos[k];
        const when = todo.querySelector('.date').innerText;
		obj[when]  = todo.querySelector('.what').innerText;
	}
	console.table(obj);
}, interval += interval);


