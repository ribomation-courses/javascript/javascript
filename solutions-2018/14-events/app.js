
function complete(ev) {
    let item = ev.target.parentNode.parentNode;
    let when = item.querySelector('.when');
    let expiry = when.querySelector('.date');
    let done = document.createElement('span');

    expiry.classList.add('completed');
    done.classList.add('done');
    when.appendChild(done);
    ev.target.setAttribute('disabled', true);
}

function edit(ev) {
    let what = document.querySelector('#what');
    let when = document.querySelector('#when');
    let item = ev.target.parentNode.parentNode;
    what.value = item.querySelector('.what').textContent.trim();
    when.value = item.querySelector('.when').textContent.trim();
}

function remove(ev) {
    let item = ev.target.parentNode.parentNode;
    document.querySelector('#items').removeChild(item);
}

function clearFields(ev) {
    document.querySelector('#what').value = '';
    document.querySelector('#when').value = new Date().toLocaleDateString();
}

function addActions(buttons) {
    for (let k = 0; k < buttons.length; ++k) {
        let b = buttons[k];
        switch (b.innerText) {
            case 'Complete':
                b.addEventListener('click', complete);
                break;
            case 'Edit':
                b.addEventListener('click', edit);
                break;
            case 'Remove':
                b.addEventListener('click', remove);
                break;
        }
    }
}

function saveTodo(ev) {
    ev.preventDefault();
    let expiry = document.querySelector('#when').value;
    let text = document.querySelector('#what').value;
    if (!text || text.trim().length === 0) return;

    let html = `
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when "><span class="date">${expiry}</span></p>
        <p class="what">${text}</p>
    `;

    let todo = document.createElement('li');
    todo.className = 'list-group-item';
    todo.innerHTML = html;
    addActions(todo.querySelectorAll('button'));

    document.querySelector('#items').appendChild(todo);
    clearFields();
}

function clearPhraseField(ev) {
    let phrase = document.querySelector('#phrase');
    phrase.value = '';

    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        items[k].style.display = 'initial';
    }
}

function interactiveSearch(ev) {
    let phrase = document.querySelector('#phrase');
    let text = phrase.value;
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        let todo = items[k];
        if (text && text.trim().length > 0) {
            let what = todo.querySelector('.what').textContent;
            todo.style.display = (what.indexOf(text) === -1) ? 'none' : 'initial';
        } else {
            todo.style.display = 'initial';
        }
    }
}

clearFields();
document.querySelector('#clear')       .addEventListener('click', clearFields);
document.querySelector('#phrase-clear').addEventListener('click', clearPhraseField);
document.querySelector('#save')        .addEventListener('click', saveTodo);
document.querySelector('#phrase')      .addEventListener('keyup', interactiveSearch);
let items = document.querySelectorAll('#items li');
for (let k = 0; k < items.length; ++k) {
    addActions( items[k].querySelectorAll('button') );
}

