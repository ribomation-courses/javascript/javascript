function delay(numSecs) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(numSecs);
        }, numSecs * 1000);
    });
}

async function run() {
    console.log('wait...');
    console.log('peek a boo, after', await delay(2), 'second(s)');
    console.log('tjolla hopp, after', await delay(1), 'second(s)');
    console.log('tjabba habba, after', await delay(2), 'second(s)');
}

run();
