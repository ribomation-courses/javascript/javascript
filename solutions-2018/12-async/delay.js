function delay(numSecs) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(numSecs);
        }, numSecs * 1000);
    });
}

console.log('wait...');
delay(2)
    .then(n => {
        console.log('peek a boo, after', n, 'second(s)');
        return delay(1);
    })
    .then(n => {
        console.log('tjolla hopp, after', n, 'second(s)');
        return delay(2);
    })
    .then(n => {
        console.log('tjabba habba, after', n, 'second(s)');
    })
;
