const FS    = require('fs');
const UTIL  = require('util');
const load  = UTIL.promisify(FS.readFile);
const store = UTIL.promisify(FS.writeFile);
const maxFontSize = 230;
const minFontSize = 20;


function randByte() {
    return Math.floor(Math.random() * 256);
}

function randHexColor() {
    return '#' + [...Array(3).keys()]
        .map(_ => randByte())
        .map(value => (value).toString(16))
        .join('');
}

function mkTags(payload, minSize, maxWords) {
    let freqs = payload.split(/[^a-z]+/i)
        .map(word => word.toLowerCase())
        .filter(word => word.length >= minSize)
        .reduce((freqs, w) => {
            freqs[w] = 1 + (+freqs[w] || 0);
            return freqs;
        }, {});

    let result = Object.entries(freqs)
        .sort((lhs, rhs) => rhs[1] - lhs[1])
        .slice(0, maxWords);

    let maxFreq = result[0][1];
    let minFreq = result[result.length - 1][1];
    let scale = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    return result
        .map(p => {
            let word = p[0];
            let freq = p[1];
            return {word: word, fontSize: Math.trunc(freq * scale)};
        })
        .map(p => {
            p.color = randHexColor();
            return p;
        })
        .sort(() => (Math.random() < 0.5) ? +1 : -1)
        .map(p => {
            return `<span style="font-size: ${p.fontSize}px; color: ${p.color}">${p.word}</span>`;
        })
        ;
}


async function run(file, minSize, maxWords) {
    let outfile = file.substring(0, file.lastIndexOf('.')) + '.html';
    let payload = await load(file, 'utf8');
    let tags    = mkTags(payload, minSize, maxWords);
    await store(outfile, `
        <html>
        <head>
            <title>Word Cloud</title>
        </head>
        <body>
            <h1>Word Cloud</h1>
            <p>${tags.join(' \n ')}</p>
        </body>
        </html>
    `, 'utf8');

    console.log('written', outfile);
}


run('./shakespeare.txt', 5, 100);

