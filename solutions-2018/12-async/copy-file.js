const FS    = require('fs');
const UTIL  = require('util');
const open  = UTIL.promisify(FS.open);
const fstat = UTIL.promisify(FS.fstat);
const read  = UTIL.promisify(FS.read);
const write = UTIL.promisify(FS.write);
const close = UTIL.promisify(FS.close);

// FS.open(filename, 'r', (err, fd) => {
//     FS.fstat(fd, (err, info) => {
//         let buf = Buffer.alloc(info.size);
//         FS.read(fd, buf, 0, buf.length, null, (err, byteCount, data) => {
//             console.log('read', byteCount, 'bytes from', filename);
//             let txt = data.toString().toUpperCase();
//             FS.close(fd, (err) => {
//                 FS.open(filename2, 'w', (err, fd) => {
//                     FS.write(fd, txt, (err, byteCount) => {
//                         FS.close(fd, (err) => {
//                             console.log('written', byteCount, 'bytes to', filename2);
//                         });
//                     })
//                 });
//             });
//         });
//     });
// });

async function copy(from, to) {
    try {
        let fd    = await open(from, 'r');
        let info  = await fstat(fd);
        let buf   = Buffer.alloc(info.size);
        let data  = await read(fd, buf, 0, buf.length, null);
        let txt   = data.buffer.toString();
        let fd2   = await open(to, 'w');
        let data2 = await write(fd2, txt.toUpperCase());
        await close(fd);
        await close(fd2);
        console.log('read   ', data.bytesRead, 'bytes from', from);
        console.log('written', data2.bytesWritten, 'bytes to  ', to);
    } catch (e) {
        console.log('*** Failed', e.code, e.syscall, e.path);
    }
}

const filename = './copy-file.js';
copy(filename, filename + '.copy');
copy('whatever.txt', filename + '.copy');
