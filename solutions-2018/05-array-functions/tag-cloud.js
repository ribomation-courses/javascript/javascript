const FS = require('fs');

let filename = './musketeers.txt';
let txt      = FS.readFileSync(filename).toString();
let N        = 100;

let freqs = txt.split(/[^a-z’]+/i)
    .map(word => word.toLowerCase())
    .filter(word => word.length >= 4)
    .reduce((freq, word) => {
        freq[word] = 1 + (+freq[word] || 0);
        return freq;
    }, {});

let maxFreq;
let minFreq;
let scale;
const tags = Object
	.entries(freqs)
    .sort((lhs, rhs) => rhs[1] - lhs[1])
    .slice(0, N)
	.map((pair, idx, arr) => {
		if (idx === 0) maxFreq = pair[1];
		if (idx === arr.length-1) minFreq = pair[1];
		return pair;
	})
	.map((pair, idx, arr) => {
		if (idx === 0) {
			let maxFont=200, minFont=30;
			scale = (maxFont-minFont) / (maxFreq-minFreq);
		}
		return pair;
	})
	.sort((_lhs, _rhs) => Math.random() < 0.5 ? -1 : +1)
	.map(p => `<span style="font-size: ${Math.floor(p[1]*scale)}pt;">${p[0]}</span>`)
    //.forEach(span => console.log(span))
	.join('\n')
	;

const htmlPrefix = `
<html>
<head> <title> Tag Cloud </title> </head>
<body>
`;	
const htmlSuffix = `
</body>
</html>
`;

FS.writeFileSync('musketeers.html', htmlPrefix + tags + htmlSuffix);
console.log('written musketeers.html');
