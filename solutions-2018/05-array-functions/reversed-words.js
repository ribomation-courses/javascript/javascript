
function reverseSentence(sentence) {
    console.log('BEFORE:', sentence);
    let result = sentence.split(/\s+/).reverse().join(' ');
    console.log('AFTER :', result);
}

reverseSentence('A horse a horse my kingdom for a horse');

