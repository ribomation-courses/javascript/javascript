const FS = require('fs');

let filename = './musketeers.txt';
let txt      = FS.readFileSync(filename).toString();
let N        = 25;

let freqs = txt.split(/[^a-z’]+/i)
    .map(word => word.toLowerCase())
    .filter(word => word.length >= 4)
    .reduce((freq, word) => {
        freq[word] = 1 + (+freq[word] || 0);
        return freq;
    }, {});

Object.keys(freqs)
    .map(w => [freqs[w], w])
    .sort((lhs, rhs) => rhs[0] - lhs[0])
    .slice(0, N)
    .forEach(p => console.log('%s: %d', p[1], p[0]));
