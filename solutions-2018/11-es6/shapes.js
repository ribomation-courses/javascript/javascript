class Shape {
    //get area() {return 0;}
}

class Rect extends Shape {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;
    }

    get area() {
        return this.width * this.height;
    }
}

class Square extends Rect {
    constructor(side) {
        super(side, side);
    }
}

class Circle extends Shape {
    constructor(radius) {
        super();
        this.radius = radius;
    }

    get area() {
        return this.radius * this.radius * Math.PI;
    }
}

class Triangle extends Shape {
    constructor(base, height) {
        super();
        this.base = base;
        this.height = height;
    }

    get area() {
        return this.base / 2 * this.height;
    }
}

let shapes = [
    new Rect(5, 10), new Square(5), new Circle(3), new Triangle(5, 4), new Shape()
];

shapes.forEach(s => {
    console.log(s, 'area = ', s.area);
});

