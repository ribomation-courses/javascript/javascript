let txt = new Date().toString();
console.log('TEXT:', txt);
console.log('LENGTH:', txt.length);

txt = txt.toUpperCase();
console.log('UPPER  :', txt);

txt = txt.replace(/\d/g, '#');   // \d = [0-9]
txt = txt.replace(/\s+/g, '');   // \s = [ \t]
console.log('REPLACE:', txt);

txt = txt.repeat(5);
console.log('REPEAT :', txt);

let N = txt.length/3;
let mid = txt.substr(N, N);
console.log('MIDPART:', mid);
