const TODO_URL = 'http://localhost:3000/todos';
const TODO_ID_KEY = 'todo-id';
let formTodoId = null;

function invokeBackend(url, method, payload) {
    let promise = new Promise((ok, err) => {
        let http = new XMLHttpRequest();
        http.open(method.toUpperCase(), url, true);
        http.setRequestHeader('Content-Type', 'application/json');
        http.onreadystatechange = () => {
            if (http.readyState === XMLHttpRequest.DONE) {
                if (http.status >= 400) {
                    err(http.status);
                } else {
                    ok(http.responseText);
                }
            }
        };
        http.send(payload ? JSON.stringify(payload) : null);
    });
    return promise;
}

function complete(ev) {
    let item = ev.target.parentNode.parentNode;
    let when = item.querySelector('.when');
    let expiry = when.querySelector('.date');
    let done = document.createElement('span');

    expiry.classList.add('completed');
    done.classList.add('done');
    when.appendChild(done);
    ev.target.setAttribute('disabled', true);
}

function edit(ev) {
    let item = ev.target.parentNode.parentNode;
    let what = document.querySelector('#what');
    let when = document.querySelector('#when');
    formTodoId = item.getAttribute(TODO_ID_KEY);
    what.value = item.querySelector('.what').textContent.trim();
    when.value = item.querySelector('.when').textContent.trim();
}

function remove(ev) {
    let item = ev.target.parentNode.parentNode;
    let id = item.getAttribute(TODO_ID_KEY);

    invokeBackend(TODO_URL + '/' + id, 'delete')
        .then(ok => {
            document.querySelector('#items').removeChild(item);
        })
        .catch(err => {
            console.error('http failed', err);
        });
}

function clearFields(ev) {
    document.querySelector('#what').value = '';
    document.querySelector('#when').value = new Date().toLocaleDateString();
    formTodoId = null;
}

function addActions(buttons) {
    for (let k = 0; k < buttons.length; ++k) {
        let b = buttons[k];
        switch (b.innerText) {
            case 'Complete':
                b.addEventListener('click', complete);
                break;
            case 'Edit':
                b.addEventListener('click', edit);
                break;
            case 'Remove':
                b.addEventListener('click', remove);
                break;
        }
    }
}

function appendTodo(when, what, id) {
    let html = `
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when "><span class="date">${when}</span></p>
        <p class="what">${what}</p>
    `;

    let todo = document.createElement('li');
    todo.setAttribute(TODO_ID_KEY, id);
    todo.className = 'list-group-item';
    todo.innerHTML = html;
    addActions(todo.querySelectorAll('button'));

    document.querySelector('#items').appendChild(todo);
}

function saveTodo(ev) {
    ev.preventDefault();
    let when = document.querySelector('#when').value;
    let what = document.querySelector('#what').value;
    if (!what || what.trim().length === 0) return;

    let url = TODO_URL;
    let method = 'post';
    if (formTodoId) {
        url += '/' + formTodoId;
        method = 'put';

        let items = document.querySelectorAll('#items li');
        let item = null;
        for (let k = 0; k < items.length; ++k) {
            if (items[k].getAttribute(TODO_ID_KEY) === formTodoId) {
                item = items[k];
                break;
            }
        }
        if (item) {
            document.querySelector('#items').removeChild(item);
        }
    }

    invokeBackend(url, method, {when: when, what: what})
        .then(data => {
            data = JSON.parse(data);
            console.info('ok', data);
            appendTodo(data.when, data.what, data.id);
            clearFields();
        })
        .catch(err => {
            console.error('http failed', err);
        });
}

function removeAllTodos(ev) {
    let items = document.querySelectorAll('#items li');
    let promises = [];
    for (let k = 0; k < items.length; ++k) {
        let id = items[k].getAttribute(TODO_ID_KEY);
        promises.push(invokeBackend(TODO_URL + '/' + id, 'delete'));
    }
    Promise.all(promises)
        .then(ok => {
            document.querySelector('#items').innerHTML = '';
        })
        .catch(err => {
            console.error('http failed', err);
        });
}

function clearSearchField(ev) {
    document.querySelector('#phrase').value = '';
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        items[k].style.display = 'initial';
    }
}

function interactiveSearch(ev) {
    let text = document.querySelector('#phrase').value;
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        let todo = items[k];
        if (text && text.trim().length > 0) {
            let what = todo.querySelector('.what').textContent;
            todo.style.display = (what.indexOf(text) === -1) ? 'none' : 'initial';
        } else {
            todo.style.display = 'initial';
        }
    }
}

function initApp(ev) {
    clearFields();
    invokeBackend(TODO_URL, 'get')
        .then(data => {
            console.info('HTTP', data);
            JSON.parse(data)
                .forEach(todo => appendTodo(todo.when, todo.what, todo.id));
        })
        .catch(err => {
            console.error('HTTP failed', err);
        })
    ;
}


document.querySelector('#clear').addEventListener('click', clearFields);
document.querySelector('#save').addEventListener('click', saveTodo);
document.querySelector('#remove-all').addEventListener('click', removeAllTodos);
document.querySelector('#phrase-clear').addEventListener('click', clearSearchField);
document.querySelector('#phrase').addEventListener('keyup', interactiveSearch);
window.addEventListener('load', initApp);

