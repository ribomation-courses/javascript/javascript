function PRINT(txt) {
    if (txt.toString) {
        txt = txt.toString();
    }

    if (this.console) {
        console.log(txt);
    } else {
        print(txt);
    }
}

function DIV() {
    PRINT( '-'.repeat(40) );
}

function Product(_name, _price, _count, _date) {
    const name  = _name || 'prod';
    let price   = _price || 42;
    let count   = _count || 0;
    const date  = _date || new Date();

    this.name  = () => {
        return name;
    };
    this.price = (value) => {
        if (value) price = value;
        return price;
    };
    this.date  = () => {
        return date;
    };
    this.count = () => {
        return count;
    };

    this.incr = () => {
        ++count;
        return this.count();
    };
    this.decr = () => {
        --count;
        if (count < 0) {
            count = 0;
            throw `*** Negative stock count for ${name}`;
        }
        return this.count();
    };

    this.toString = () => {
        return `Product{${name}, ${price} kr, ${count} items, ${date.toLocaleDateString()}}`;
    };
}

let products = [
    new Product('apples ',  5, 10),
    new Product('bananas', 10,  5),
    new Product('lemons ', 15,  3),
    new Product('oranges', 12,  0),
];

products.forEach(p => PRINT(p));

DIV();
products.forEach(p => {
    let n = 5;
    try {
        while (n-- > 0) { p.decr(); }
    } catch (e) { PRINT(e); }
});
DIV();

products.forEach(p => PRINT(p));

