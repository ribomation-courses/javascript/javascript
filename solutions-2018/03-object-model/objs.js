function log(txt) {
    if (this.console) {
        console.log(txt);
    } else {
        print(txt);
    }
}
function dump(obj) {
    log(JSON.stringify(obj, null, 2));
}


function Shape(name, x, y) {
    this.name = name || 'shape';
    this.x    = x || 0;
    this.y    = y || 0;

    var t      = 42;
    this.thick = function (value) {
        if (value) t = value;
        return t;
    };
}

var sh1 = new Shape('sh1', 2, 5);
dump(sh1);

log('--- can access "this.prop"');
log('sh1.x: ' + sh1.x);
sh1.x = 5;
dump(sh1);

log('--- cannot access "var prop"');
log('sh1.t: ' + sh1.t);
log('--- but indirectly via its getter');
log('sh1.thick: ' + sh1.thick());


log('--- 2nd shape');
var sh2 = new Shape('sh2', 5, 10);
dump(sh2);

log('--- private prop, different values');
sh2.thick(21);
log('sh1.thick: ' + sh1.thick());
log('sh2.thick: ' + sh2.thick());

log('--- own prop');
sh1.msg = 'just for me';
dump(sh1);
dump(sh2);

log('--- shared prop');
Shape.prototype.color = 'red';
log('sh1.color: ' + sh1.color);
log('sh2.color: ' + sh2.color);
dump(sh1);
dump(sh2);

log('--- shared prop, but different values');
sh2.color = 'blue';
log('sh1.color: ' + sh1.color);
log('sh2.color: ' + sh2.color);

log('--- no method move');
try {
    sh1.move(10, 10);
} catch (x) {
    log(x.toString());
}

log('--- method move added');
Shape.prototype.move = function (dx, dy) {
    this.x += dx;
    this.y += dy;
    log('moved to pos{' + this.x + ', ' + this.y + '}');
};
sh1.move(10, 10);
dump(sh1);
sh2.move(25, 25);
dump(sh2);

log('--- late methods cannot access private props');
Shape.prototype.dummy = function () {
    log('thickness: ' + this.t);
    log('getThickness: ' + this.thick());
};
sh1.dummy();

log('--- inheritance');
function Rect(name, x, y, w, h) {
    Shape.call(this, name, x, y);
    this.w = w || 0;
    this.h = h || 0;
}
Rect.prototype             = Object.create(Shape.prototype);
Rect.prototype.constructor = Rect;
Rect.prototype.superClass  = Shape.prototype;

var r1 = new Rect('r1', 2, 2, 5, 8);
dump(r1);
r1.move(2, 2);


log('--- override');
Shape.prototype.area = function () {
    return 0;
};
Rect.prototype.area  = function () {
    return this.w * this.h;
};
log('sh1.area: ' + sh1.area());
log('r1.area: ' + r1.area());


log('--- meta-programming helper function added');
Function.prototype.inheritsFrom = function (superClass) {
    this.prototype             = Object.create(superClass.prototype);
    this.prototype.constructor = this;
    this.prototype.superClass  = superClass.prototype;
};

log('--- Circ using helper');
function Circ(name, x, y, r) {
    Shape.call(this, name, x, y);
    this.r = r || 0;
}
Circ.inheritsFrom(Shape);
Circ.prototype.area = function () {
    return this.r * this.r * Math.PI;
};

var c1 = new Circ('c1', 1, 1, 3);
dump(c1);
log('c1.area: ' + c1.area());
c1.move(4, 4);
dump(c1);

log('--- invoking super-class props');
Shape.prototype.toString = function () {
    return this.name + ': ' + 'x=' + this.x + ', y=' + this.y
};
Rect.prototype.toString  = function () {
    return this.superClass.toString.call(this) + ', w=' + this.w + ', h=' + this.h;
};
Circ.prototype.toString  = function () {
    return this.superClass.toString.call(this) + ', r=' + this.r;
};

log('sh1.string: ' + sh1.toString());
log('r1.string: ' + r1.toString());
log('c1.string: ' + c1.toString());

log('--- list of shapes');
var shapes = [sh1, r1, c1, sh2];
shapes.forEach(function (s) {
    log(s.toString() + ', area=' + s.area());
});

