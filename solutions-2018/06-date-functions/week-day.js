
function weekday(n) {
    switch (n) {
        case 0: return 'söndag';
        case 1: return 'måndag';
        case 2: return 'tisdag';
        case 3: return 'onsdag';
        case 4: return 'torsdag';
        case 5: return 'fredag';
        case 6: return 'lördag';
    }
}

const dateTxt = '2018-11-26';
const date = new Date(dateTxt);
console.log(`Datumet '${dateTxt}' är en ${weekday(date.getDay())}`);
