function Product(_name, _price, _outOfStock) {
    this.name = _name;
    this.price = _price;
    this.outOfStock = _outOfStock;
}

let p1 = new Product('JavaScript 4 Beginners', 199.95, false);
let p2 = new Product('JavaScript Primer', 149.95, true);

console.log('p1:', p1);
console.log('p2:', p2);

console.log('p1.keys:', Object.keys(p1));
console.log('p2.keys:', Object.keys(p2));

console.log('p1.values:', Object.values(p1));
console.log('p2.values:', Object.values(p2));

console.log('p1.entries:', Object.entries(p1));
console.log('p2.entries:', Object.entries(p2));
