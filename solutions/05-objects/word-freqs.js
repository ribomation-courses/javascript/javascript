const fs = require('fs');

const filename = process.argv[2] || process.argv[1];
const maxWords = +process.argv[3] || 10;
const minWord  = +process.argv[4] || 5;

const freqs = fs
    .readFileSync(filename)
    .toString()
    .toLowerCase()
    .split(/[^a-z’]+/i)
    .filter(w => w.length >= minWord)
    .reduce((freq, word) => {
        freq[word] = 1 + (+freq[word] || 0);
        return freq;
    }, {})
;

Object.entries(freqs)
    .sort((lhs, rhs) => rhs[1] - lhs[1])
    .slice(0, maxWords)
    .forEach(pair => console.log(`${pair[0]}: ${pair[1]}`))
;
