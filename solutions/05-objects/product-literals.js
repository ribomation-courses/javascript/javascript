let p1 = {
    name: 'JavaScript 4 Beginners',
    price: 149.95,
    outOfStock: false
};

let p2 = {
    name: 'JavaScript Primer',
    price: 199.95,
    outOfStock: true
};

console.log('p1:', p1);
console.log('p2:', p2);

console.log('p1.keys:', Object.keys(p1));
console.log('p2.keys:', Object.keys(p2));

console.log('p1.values:', Object.values(p1));
console.log('p2.values:', Object.values(p2));

console.log('p1.entries:', Object.entries(p1));
console.log('p2.entries:', Object.entries(p2));
