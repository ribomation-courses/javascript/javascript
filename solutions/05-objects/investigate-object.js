let p = Object.freeze({
    name: 'JavaScript 4 Beginners',
    price: 149.95,
    outOfStock: false
});

console.log('p:', p);

p.color = 'blue';
console.log('p:', p);

delete p.outOfStock;
console.log('p:', p);

let p1 = 'JavaScript 4 Beginners';
let p2 = 'JavaScript 4 Beginners';
console.log('p1 === p2?', Object.is(p1, p2));

let p3 = Object.assign({}, {name:'JS'}, {name2:'TS', price:123});
console.log('p3:', p3);
