const fs = require('fs');
const filename = './cars.csv';

class Car {
    constructor() {}
    static Builder(){return new Car();}
    Id(value)       {this._id       = parseInt(value); return this;}
    Producer(value) {this._producer = value;           return this;}
    Model(value)    {this._model    = value;           return this;}
    Year(value)     {this._year     = parseInt(value); return this;}
    Vin(value)      {this._vin      = value;           return this;}
    City(value)     {this._city     = value;           return this;}

    get id()       {return this._id;}
    get producer() {return this._producer || '-';}
    get model()    {return this._model    || '-';}
    get year()     {return this._year     || new Date().getFullYear();}
    get vin()      {return (this._vin     || '*').toUpperCase();}
    get city()     {return this._city     || '-';}

    toString() {
        return `Car[${this.producer} ${this.model} (${this.year}). VIN=${this.vin} @ ${this.city}]`;
    }
}

fs
    .readFileSync(filename)
    .toString()
    .split(/\r?\n/g) // A? A* A+ A{2,5}
    .slice(1)
    .map(line => line.split(/,/g))
    .map(csv => Car.Builder() //id,producer,model,year,vin,city
                   .Id(csv[0])
                   .Producer(csv[1])
                   .Model(csv[2])
                   .Year(csv[3])
                   .Vin(csv[4])
                   .City(csv[5])
                   )
    .filter(car => !Number.isNaN(car.id))
    .sort((lhs, rhs) => rhs.year - lhs.year)
    .forEach(car => console.log(car.toString()))
;
