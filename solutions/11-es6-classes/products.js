class Product {
    constructor(name, price, inStock) {
        this._name = name;
        this._price = price;
        this._inStock = inStock;
    }
    toString() {
        return `Product[${this.name}, ${this.price} kr${this.inStock ? '': ', Out of Stock'}]`;
    }
    raisePrice(amount) {
        this._price += amount;
        return this._price;
    }
    get name() {return this._name;}
    get price() {return this._price;}
    get inStock() {return this._inStock;}
    set inStock(val) {this._inStock = (val === 'true'); return this.inStock;}
}

const products = [
    new Product('Apple', 5, true),
    new Product('Banana', 4.55, true),
    new Product('Orange', 7.25, false),
    new Product('Lemon', 2.35, true),
];

products.forEach(p => console.log(p.toString()));

