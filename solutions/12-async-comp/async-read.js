const fs = require('fs');
const path = require('path');

function slurp(filename) {
    const name = path.basename(filename);
    console.log('[before] readFile', name);

    fs.readFile(filename, (err, data) => {
        if (err) {
            console.error('***', 'cannot open', filename);
            return;
        }
        console.log('[enter] readFile', name);
        const payload = data
            .toString()
            .substr(0, 64)
            .toUpperCase()
            .replace(/\s+/g, '#')
            ;
        console.log(payload, '...');
        console.log('[exit] readFile', name);
    });

    console.log('[after] readFile', name);
}

slurp(process.argv[2] || process.argv[1]);
slurp('no-such-file.txt');
