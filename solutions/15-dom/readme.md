# Todo App

## Preparation
Download the NPM packages.

    npm install

## Launch Client
Start the client in a terminal.

    npm run start

## Usage
Open the _web developer_ app of the browser and the _Console_ tab.
Inspect the console print-outs.

![Console print-outs](./print-outs.png)
