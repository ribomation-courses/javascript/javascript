
function dump(name, items) {
    console.log(`--- All ${name} ---`);
    for (let item of items) console.log('item', item);
}

function listButton() {
    const items = document.querySelectorAll('button');
    dump('Buttons', items);
}

function listInputFields() {
    const items = document.querySelectorAll('input');
    dump('Input Fields', items);
}

function listTodoItems() {
    const items = document.querySelectorAll('#items li');
    dump(' Todo Items', items);
}

function listWhenItems() {
    const items = document.querySelectorAll('#items li .date');
    let dates = [];
    for (let item of items) {
        const date = new Date(item.innerText);
        date.setHours(10);
        date.setMinutes(15);
        date.setSeconds(0);
        dates.push( date.toLocaleString() );
    }
    dump(' When Items', dates);
}

function isOverdue(date) {
    return date.getTime() < new Date().getTime();
}

function setOverdue() {
    console.log(`--- set Overdue ---`);
    const items = document.querySelectorAll('#items li');
    for (let item of items) {
        const date = item.querySelector('.date');
        if (isOverdue(new Date(date.innerText))) {
            const when = item.querySelector('.when');
            when.classList.add('overdue');
            console.log('  *', 'when', when);
        }
    }
}

function setCompleted() {
    console.log(`--- set Completed ---`);
    const items = document.querySelectorAll('#items li');
    for (let item of items) {
        const date      = item.querySelector('.date');
        const overdue   = isOverdue(new Date(date.innerText));
        const completed = date.classList.contains('completed');
        if (!overdue && !completed) {
            date.classList.add('completed');
            const when = item.querySelector('.when');

            const done = document.createElement('span');
            done.classList.add('done');
            when.appendChild(done);

            console.log('  *', 'date', date);
        }
    }
}

function insertTodo() {
    console.log(`--- insert Todo ---`);
    const when = new Date().toLocaleDateString();
    const what = 'Succeed to insert a new todo item';
    
    const todo  = document.createElement('li');
    document.querySelector('#items').appendChild(todo);
    todo.outerHTML = `
        <li class="list-group-item">
            <div class="float-right">
                <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
                <button type="button" class="btn btn-primary btn-sm">Edit</button>
                <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
            </div>
            <p class="when "><span class="date">${when}</span></span></p>
            <p class="what">${what}</p>
        </li>
    `;

    console.log('  *', 'todo', todo);
}


let delay = 0;
let delta = 5;

console.log('Listing and Manipulation of Elements...');

const actions = [
    listButton, listInputFields, listTodoItems, listWhenItems,
    setOverdue, setCompleted, insertTodo
];
actions.forEach(action => {
    setTimeout(action, (delay += delta) * 1000);
});

