
let multiplier = 10;

module.exports.factor   = (f) => multiplier = f;
module.exports.multiply = (x) => multiplier * x;
