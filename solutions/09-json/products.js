const productsCSV = `
id;name;price;isOutOfStock;created
1;Sierra 3500;749.06;false;2018/10/14
2;Esprit;271.77;false;2018/09/11
3;Discovery Series II;813.69;false;2018/06/27
4;XJ Series;268.26;false;2018/02/12
5;Nubira;703.32;false;2018/10/26
6;Neon;744.15;true;2019/03/12
7;Mark VIII;997.12;true;2019/01/07
8;Festiva;992.86;false;2019/02/01
9;Aerostar;383.72;true;2018/04/13
10;Mazda6;799.24;false;2018/01/31
`.trim();

const products = productsCSV
    .split(/\r?\n/)
    .slice(1)
    .map(csv => csv.split(/;/))
    .map(fields => {
        return {
            id: fields[0],
            name: fields[1],
            price: Number.parseFloat(fields[2]),
            isOutOfStock: fields[3] === 'true',
            created: new Date(fields[4])
        };
    });

console.log(JSON.stringify(products, null, 2));

