const LIB = require('../lib');

describe('simple', () => {
    it('should return Nisse, when passing Nisse', () => {
        const res = LIB.greeting('Nisse');
        expect(res).toBe('Hello Nisse');
    });

    it('should return NoName, when passing nothing', () => {
        expect(LIB.greeting()).toBe('Hello NoName');
    });
});
