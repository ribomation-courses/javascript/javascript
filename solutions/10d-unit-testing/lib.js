function greeting(name) {
    name = name || 'NoName';
    return `Hello ${name}`;
}

module.exports.greeting = greeting;

