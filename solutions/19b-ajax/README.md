# Simple Todo List app

## Preparation

1. Create a new folder for the project and a nested `src/` folder.
1. Copy the files from `gitlab/start-code/todo-2/*` into the project folder.
1. Start Visual Code in the project folder

## Setup
Open a terminal and install all dependencies

    npm install

## Run DEV
Launch first the json server and then dev server

    npm run srv
    npm run dev

## Build & Run PROD
You can also run the system in production mode 
(_i.e. no auto-reload but optimized code_).
First build the app, then launch the backend server
followed by the production server.

_N.B._) <br>
The production server is using HTTP2 with a _self-signed_
certificate, which You need have to grant, in order to proceed
to the web app. 
[Follow the instruction here to make this more permanent](https://github.com/lwsjs/local-web-server/wiki/How-to-get-the-%22green-padlock%22-using-the-built-in-certificate).

    npm run build
    npm run srv
    npm run prod

