import { Todo } from "./todo.mjs";

export class BackendService {
    constructor(baseUrl) {
        this._baseUrl = baseUrl;
    }

    getAll() {
        return this._invokeBackend(this.url())
            .then(data => Promise.resolve(data.map(t => Todo.fromExternal(t))));
    }

    read(id) {
        return this._invokeBackend(this.url(id))
            .then(data => Todo.fromExternal(data));
    }

    create(when, what) {
        let payload = { when, what, completed: false };
        return this._invokeBackend(this.url(), 'post', payload)
            .then(data => Todo.fromExternal(data));
    }

    update(id, when, what, completed = false) {
        let payload = { when, what, completed };
        return this._invokeBackend(this.url(id), 'put', payload)
            .then(data => Todo.fromExternal(data));
    }

    remove(id) {
        return this._invokeBackend(this.url(id), 'delete');
    }

    url(id) {
        if (id) return `${this._baseUrl}/${id}`;

        return this._baseUrl;
    }

    _invokeBackend(url, method = 'get', payload = undefined) {
        console.log(`invokeBackend: ${method.toUpperCase()} ${url} ${payload ? ': ' + JSON.stringify(payload) : ''}`);

        return new Promise((ok, err) => {
            let http = new XMLHttpRequest();

            http.open(method.toUpperCase(), url, true);
            http.setRequestHeader('Content-Type', 'application/json');

            http.onreadystatechange = () => {
                if (http.readyState === XMLHttpRequest.DONE) {
                    if (http.status >= 400) {
                        err(http.status);
                    } else {
                        let data = JSON.parse(http.responseText);
                        ok(data);
                    }
                }
            };

            if (payload) {
                payload = JSON.stringify(payload);
                http.send(payload);
            } else {
                http.send();
            }
        });
    }

}

