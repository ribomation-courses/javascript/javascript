import { Todo } from './todo';
import { BackendService } from './backend-service';

let currentId = undefined;
let todoList = [];
const svc = new BackendService('http://localhost:3000/todos');

window.addEventListener('load', () => {
    initForm();
    initSearch();
    loadTodos();
});

function initForm() {
    document.querySelector('#cancel')
        .addEventListener('click', (ev) => { cancel(); });

    document.querySelector('#save')
        .addEventListener('click', (ev) => { save(); });

    cancel();
}

function initSearch() {
    let search = document.querySelector('#search');

    search.addEventListener('input', (ev) => {
        let phrase = search.value.toLowerCase();
        console.log('phrase', `"${phrase}"`);

        if (phrase.trim().length === 0) {
            populate(todoList);
        } else {
            let result = todoList.filter(t => t.what.toLowerCase().includes(phrase));
            populate(result);
        }
    });
}

function loadTodos() {
    svc.getAll().then(todos => {
        console.log('getAll:', todos);

        todoList = todos;
        populate(todoList);
    });
}

function populate(todoList, overdueDate = new Date()) {
    let items = document.querySelector('.items ul');
    items.innerHTML = '';

    todoList.forEach(todo => {
        if (!todo.completed && todo.when < overdueDate) { 
            todo.overdue = true; 
        }
        let todoDOM = todo.toDOM();

        todoDOM.querySelector('button.complete')
            .addEventListener('click', (ev) => { complete(todo); });

        todoDOM.querySelector('button.edit')
            .addEventListener('click', (ev) => { edit(todo); });

        todoDOM.querySelector('button.remove')
            .addEventListener('click', (ev) => { remove(todo); });

        items.appendChild(todoDOM);
    });
}

function save() {
    let when = document.querySelector('#when').value;
    let what = document.querySelector('#what').value;

    if (!when || when.toString().trim().length === 0) {
        alert('Date must not be empty or invalid');
        return;
    }

    if (!what || what.toString().trim().length === 0) {
        alert('What must not be empty');
        return;
    }

    if (currentId) { //update
        svc.update(currentId, when, what).then(todo => {
            console.log('update:', todo);
            toast(`Updated todo ID=${todo.id}`);
            refreshTodo(todo);
            cancel();
        });
    } else { //create
        svc.create(when, what).then(todo => {
            console.log('create:', todo);
            toast(`Created new todo ID=${todo.id}`);
            todoList.push(todo);
            populate(todoList);
            cancel();
        })
    }
}

function cancel() {
    let when = document.querySelector('#when');
    let what = document.querySelector('#what');

    when.value = new Date().toLocaleDateString();
    what.value = '';
    currentId = undefined;
}

function complete(todo) {
    todo.completed = !todo.completed;

    svc.update(todo.id, todo.when, todo.what, todo.completed)
        .then(todo => {
            refreshTodo(todo);
        });
}

function edit(todo) {
    let when = document.querySelector('#when');
    let what = document.querySelector('#what');

    when.value = todo.when.toLocaleDateString();
    what.value = todo.what;
    currentId = todo.id;
}

function remove(todo) {
    todoList = todoList.filter(t => t.id !== todo.id);
    populate(todoList);

    svc.remove(todo.id)
        .then(_ => {
            toast(`Todo ID=${todo.id} removed`);
        });
}

function refreshTodo(todo) {
    let t = todoList.find(t => t.id === todo.id);
    if (!t) throw new Error('unexpected: missing ID=' + todo.id);

    t.when = todo.when;
    t.what = todo.what;
    t.completed = todo.completed;

    populate(todoList);
}

function toast(msg) {
    let pane = document.querySelector('.notification .message');
    pane.innerHTML = msg;
    pane.classList.add('show');

    setTimeout(() => {
        pane.classList.remove('show');
    }, 2000);
}

