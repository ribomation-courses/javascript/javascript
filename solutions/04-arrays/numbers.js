
let ints = n => Array.from({length: n}, (_, i) => i+1);
console.log(ints(10));

let odds = n => Array.from({length: n}, (_, i) => 2*i+1);
console.log(odds(10));
