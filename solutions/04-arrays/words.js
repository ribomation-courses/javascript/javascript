const fs = require('fs');

fs
    .readFileSync(process.argv[2] || process.argv[1])
    .toString()
    .split(/[^a-z]+/i)
    .sort((lhs, rhs) => rhs.length - lhs.length)
    .filter((elem, idx, arr) => arr.indexOf(elem) === idx)  //uniq
    .slice(0, 10)
    .forEach(w => console.log(w))
    ;
