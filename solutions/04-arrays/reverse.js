
const sentence = process.argv[2] || 'Foo bar strikes again'
const result   = sentence.split(/ /g).reverse().join(' ');
console.info(sentence, ' -> ', result);
