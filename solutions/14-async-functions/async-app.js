const path = require('path');
const io = require('./async-io');

async function copyFile(fromFilename, toFilename) {
    try {
        let contents = await io.readByPromise(fromFilename);
        let payload = contents.replace(/\s+/g, ':').slice(0, 100).toUpperCase();
        await io.writeByPromise(toFilename, payload);
        
        console.log(`written ${contents.length} bytes to ${toFilename}`);
    } catch (x) {
        console.log('***', x.message);
    }
}

const filename = process.argv[2] || process.argv[1];
copyFile(filename, 'x-' + path.basename(filename) + '.tmp');

copyFile('no-such-file.txt');

