const fs = require('fs');

function readByPromise(filename) {
    return new Promise((success, failure) => {
        fs.readFile(filename, (err, data) => {
            if (err) {
                failure(err);
            } else {
                success(data.toString());
            }
        });
    });
}

function writeByPromise(filename, contents) {
    return new Promise((success, failure) => {
        fs.writeFile(filename, contents, (err) => {
            if (err) {
                failure(err);
            } else {
                success('ok');
            }
        });
    });
}

module.exports = {readByPromise, writeByPromise};
