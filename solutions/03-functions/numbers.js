function sum(n) {
    if (n <= 1) return 1;
    return n + sum(n-1);
}
function prod(n) {
    if (n <= 1) return 1;
    return n * prod(n-1); 
}
function prod2(n) {
    let result = 1;
    for (let k=1; k<=n; ++k) result = result * k;
    return result;
}
function fib(n) {
    if (n <= 2) return 1;
    return fib(n-2) + fib(n-1);
}

function table(N) {
    console.log(`k \tsum(k) \tfib(k) \tprod(k)`);
    for (let k=1; k<=N; ++k) {
        console.log(`${k} \t${sum(k)} \t${fib(k)} \t${prod(k)}`);
    }
}

table(+(process.argv[2]) || 10);

/*
prod(3) ->
3 * prod(2) ->
3 * 2 * prod(1) ->
3 * 2 * 1
3 * 2
6
*/

