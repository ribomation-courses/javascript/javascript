const N = +(process.argv[2]) || 20 ;

for (let k = 1; k <= N; ++k) {
    let output = k;
    if (k % 3 === 0) output = 'Fizz';
    if (k % 5 === 0) output = 'Buzz';
    if (k % 3 === 0 && k % 5 === 0) output = 'FizzBuzz';
    console.log(`(${k}):`, output);
}
