import { Todo } from './todo';

function populate(todoList, overdueDate = new Date()) {
    //1. grab the items UL
    let items = document.querySelector('.items ul');

    //2. for-each todo, append to items-UL, using todo.toDOM()
    todoList.forEach(todo => {
        //set item.overdue, if its when date is before overdueDate
        if (todo.when < overdueDate) {
            todo.overdue = true;
        }
        items.appendChild(todo.toDOM());
    });
}

function init() {
    //set form date to today's date, in the form 'yyyy-mm-dd'
    //1. grab #when, assign to variable when
    //2. set when.value to date formatted

    let when = document.querySelector('#when');
    when.value = new Date().toLocaleDateString();
}

let items = [
    //add a bunch of todo items
    new Todo(1, '2019-06-20', 'Köpa sill och nubbe'),

    //ensure at least one is completed
    new Todo(2, '2019-06-12', 'Börja JS kursen', true),

    //ensure at least one is overdue
    new Todo(3, '2019-05-27', 'Köpa solhatt')
];

init();
populate(items, new Date('2019-06-01'));


