import { Todo } from './todo';

function populate(todoList, overdueDate = new Date()) {
    let items = document.querySelector('.items ul');
    items.innerHTML = '';

    todoList.forEach(todo => {
        if (todo.when < overdueDate) {
            todo.overdue = true;
        }
        let todoDOM = todo.toDOM();

        let complete = todoDOM.querySelector('button.complete');
        complete.addEventListener('click', (ev) => {
            todo.completed = !todo.completed;
            storeTodos(todoList);

            let when = todoDOM.querySelector('p.when');
            when.classList.toggle('completed');
        });

        let edit = todoDOM.querySelector('button.edit');
        edit.addEventListener('click', (ev) => {
            let when = document.querySelector('#when');
            let what = document.querySelector('#what');

            when.value = todo.when.toLocaleDateString();
            what.value = todo.what;
            currentId = todo.id;
        });

        let remove = todoDOM.querySelector('button.remove');
        remove.addEventListener('click', (ev) => {
            console.log('clicked remove', todo.id);
            const ID = todo.id;
            todoList = todoList.filter(t => t.id !== ID);

            storeTodos(todoList);
            populate(todoList);
        });

        items.appendChild(todoDOM);
    });
}

function clearForm() {
    let when = document.querySelector('#when');
    when.value = new Date().toLocaleDateString();

    let what = document.querySelector('#what');
    what.value = '';
    currentId = undefined;
}

function initForm() {
    let cancel = document.querySelector('#cancel');
    cancel.addEventListener('click', (ev) => {
        clearForm();
    });

    let save = document.querySelector('#save');
    save.addEventListener('click', (ev) => {
        let when = document.querySelector('#when');
        let what = document.querySelector('#what');

        if (currentId === undefined) {//new
            let ID = maxId(todoList) + 1;
            let todo = new Todo(ID, when.value, what.value);
            todoList.push(todo);
        } else {//edit
            let todo = todoList.find(t => t.id === currentId)
            todo.when = when.value;
            todo.what = what.value;
        }
        storeTodos(todoList);
        populate(todoList);
        clearForm();
    });

    clearForm();
}

function maxId(itemList) {
    let ids = itemList.map(t => t.id);
    return Math.max(...ids);
}

function initSearch() {
    let search = document.querySelector('#search');

    search.addEventListener('keyup', (ev) => {
        let phrase = search.value.toLowerCase();
        //console.log('phrase', `"${phrase}"`);
        if (phrase.trim().length === 0) {
            populate(todoList);
        } else {
            let result = todoList.filter(t => t.what.toLowerCase().includes(phrase));
            populate(result);
        }
    });

    search.addEventListener('change', (ev) => {
        populate(todoList);
    });
}

let currentId = undefined;
let todoList = [
    new Todo(1, '2019-06-20', 'Köpa sill och nubbe'),
    // new Todo(2, '2019-06-12', 'Börja JS kursen', true),
    // new Todo(3, '2019-05-27', 'Köpa solhatt')
];
//console.info('todos:', JSON.stringify(todoList));

initForm();
initSearch();
//populate(todoList);

function loadTodos() {
    let todoJson = localStorage.getItem('todo.list');
    if (todoJson) {
        return JSON.parse(todoJson).map(t => Todo.create(t))
    }
    return null;
}

function storeTodos(todoList) {
    localStorage.setItem('todo.list', JSON.stringify(todoList));
}

window.addEventListener('load', () => {
    let todos = loadTodos();
    if (!todos) return;

    console.log('saved todos:', todos);
    todoList = todos;
    populate(todoList);
});


