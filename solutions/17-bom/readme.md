# Todo App with Actions

## Preparation
Download the NPM packages.

    npm install

## Launch Client
Start the client in a terminal.

    npm run start

## Usage
Create one or more items and inspect them in the _web developer_ app of the browser. 
Open the _Application_ tab and then select _Local Storage_.

![Local Storage](./local-storage.png)

