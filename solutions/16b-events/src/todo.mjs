
export class Todo {
    constructor(_id, _when, _what, _completed) {
        this.id = _id || 1;
        this.when = _when;
        this.what = _what;
        this.completed = _completed || false;
    }

    get id() { return this._id; }
    get when() { return this._when; }
    get what() { return this._what; }
    get completed() { return this._completed; }
    get overdue() { return this._overdue; }

    set id(val) { this._id = val; }
    set when(val) { 
        if (val && typeof val === 'string') {
            this._when = new Date(val);
        } else if (val && typeof val === 'number') {
            this._when = new Date(val);
        } else if (val && val instanceof Date) {
            this._when = val;
        } else {
            throw new Error('invalid date: '+val);
        }
    }
    set what(val) { this._what = val; }
    set completed(val) { this._completed = (val === true); }
    set overdue(val) { this._overdue = (val === true); }

    toDOM() {
        let li = document.createElement('li');
        li.setAttribute('item-id', this.id);
        li.innerHTML = `
            <div class="buttons">
                <button type="button" class="complete">Complete</button>
                <button type="button" class="edit">Edit</button>
                <button type="button" class="remove">Delete</button>
            </div>
            <p class="when ${this.overdue ? 'overdue' : ''} ${this.completed ? 'completed' : ''}">${this.when.toLocaleDateString()}</p>
            <p class="what">${this.what}</p>
        `;
        return li;
    }
}
