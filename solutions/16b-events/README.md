# Simple Todo List app

## Preparation

1. Create a new folder for the project and a nested `src/` folder.
1. Copy the files from `gitlab/start-code/todo-2/*` into the project folder.
1. Start Visual Code in the project folder

## Setup
Open a terminal and install all dependencies

    npm install

## Run DEV
Launch the dev server, with the command

    npm run dev

## Build & Run PROD
First build and then launch the server

    npm run build
    npm run prod

