function weekday(value) {
    if (value === undefined || value === null) {
        value = new Date()
    } else if (typeof value === 'string') {
        value = new Date(value)
    } else if (value instanceof Date) {
        value = value
    } else if (typeof value === 'number') {
        value = new Date(value)
    } else {
        throw new Error('Invalid input: "' + JSON.stringify(value) + '"')
    }

    const dayNames = [
        'söndag','måndag','tisdag','onsdag',
        'torsdag','fredag','lördag'
    ];
    return dayNames[value.getDay()]
}

const currentYear = new Date().getFullYear();
const timestamp = 1556928000000;

console.log('idag är det', weekday());
console.log('1 maj i år var en', weekday(currentYear + '-05-01'));
console.log('1 maj för 3 år sedan var en', weekday((currentYear-3) + '-05-01'));
console.log('senaste nyårsafton var en', weekday(new Date((currentYear-1) + '-12-31')))
console.log('denna dag', timestamp, 'är en', weekday(timestamp))

try {
    console.log('invalid', weekday(true))
} catch(err) {console.log('*** ', err.message)}
try {
    console.log('invalid', weekday({val:42}))
} catch(err) {console.log('*** ', err.message)}
try {
    console.log('invalid', weekday([42]))
} catch(err) {console.log('*** ', err.message)}
