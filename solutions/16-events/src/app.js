
function clearPhrase() {
    const phrase = document.querySelector('#phrase');
    phrase.value = '';
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        items[k].style.display = 'initial';
    }
}

function clear() {
    const what = document.querySelector('#what');
    const when = document.querySelector('#when');

    what.value = '';
    when.value = new Date().toLocaleDateString();
}

function save(ev) {
    ev.preventDefault();
    const what = document.querySelector('#what').value;
    const when = document.querySelector('#when').value;

    const todo  = document.createElement('li');
    todo.classList.add('list-group-item');
    todo.innerHTML = `
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1">Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when "><span class="date completed">${when}</span></p>
        <p class="what">${what}</p>
    `;

    document.querySelector('#items').appendChild(todo);
    addActions(todo.querySelectorAll('button'));
}

function complete(ev) {
    let item = ev.target.parentNode.parentNode;
    let when = item.querySelector('.when');
    let expiry = when.querySelector('.date');
    let done = document.createElement('span');

    expiry.classList.add('completed');
    done.classList.add('done');
    when.appendChild(done);
    ev.target.setAttribute('disabled', true);
}

function edit(ev) {
    let item = ev.target.parentNode.parentNode;
    let what = document.querySelector('#what');
    let when = document.querySelector('#when');
    what.value = item.querySelector('.what').textContent.trim();
    when.value = item.querySelector('.when').textContent.trim();
}

function remove(ev) {
    let item = ev.target.parentNode.parentNode;
    document.querySelector('#items').removeChild(item);
}

function addActions(buttons) {
    for (let k = 0; k < buttons.length; ++k) {
        let b = buttons[k];
        switch (b.innerText) {
            case 'Complete':
                b.addEventListener('click', complete);
                break;
            case 'Edit':
                b.addEventListener('click', edit);
                break;
            case 'Remove':
                b.addEventListener('click', remove);
                break;
        }
    }
}

function interactiveSearch(ev) {
    let phrase = document.querySelector('#phrase');
    let text = phrase.value.toLowerCase();
    let items = document.querySelectorAll('#items li');
    for (let k = 0; k < items.length; ++k) {
        let todo = items[k];
        if (text && text.trim().length > 0) {
            let what = todo.querySelector('.what').textContent.toLowerCase();
            todo.style.display = (what.indexOf(text) === -1) ? 'none' : 'initial';
        } else {
            todo.style.display = 'initial';
        }
    }
}

clear();
document.querySelector('#phrase-clear').addEventListener('click', clearPhrase);
document.querySelector('#clear').addEventListener('click', clear);
document.querySelector('#save').addEventListener('click', save);
document.querySelector('#phrase').addEventListener('keyup', interactiveSearch);
let items = document.querySelectorAll('#items li');
for (let k = 0; k < items.length; ++k) {
    addActions( items[k].querySelectorAll('button') );
}
