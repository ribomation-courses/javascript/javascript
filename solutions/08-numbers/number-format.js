const input = 123456.7890;

console.log('raw', input);
console.log('toString', input.toString());
console.log('toLocaleString', input.toLocaleString());
console.log('locale en', input.toLocaleString('en', {style:'currency', currency:'EUR'}));
console.log('locale sv', input.toLocaleString('sv', {style:'currency', currency:'EUR'}));

console.log('Intl', new Intl.NumberFormat('sv', {
    style: 'currency', 
    currency: 'SEK', 
    currencyDisplay: 'symbol', 
    useGrouping:true
}).format(input));

// Chrome JS Console:
// input.toLocaleString('en', {style:'currency', currency:'EUR'})
// --> "€123,456.79"
// input.toLocaleString('sv', {style:'currency', currency:'EUR'})
// --> "123 456,79 €"
