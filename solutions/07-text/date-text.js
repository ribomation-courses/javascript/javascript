const input = new Date().toString();

console.log('input        :', input);
console.log('input.length :', input.length);
console.log('input.upper  :', input.toUpperCase());
console.log('input.replace:', input.replace(/\d/g, '#').replace(/\s/g, ''));
console.log('input.repeat :', input.repeat(5));
console.log('input.substr :', input.substr(input.length/3, input.length/3));
