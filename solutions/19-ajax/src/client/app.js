'use strict';
const BASE_URL = 'http://localhost:3000/todos/';
const ID_KEY = 'todo-id';
let formTodoId = undefined;


function invokeBackend(url, method, payload) {
    let promise = new Promise((success, failure) => {
        let http = new XMLHttpRequest();

        http.open(method.toUpperCase(), url, true);
        http.setRequestHeader('Content-Type', 'application/json');

        http.onreadystatechange = () => {
            if (http.readyState === XMLHttpRequest.DONE) {
                switch (http.status) {
                    case 200: //OK
                        success(JSON.parse(http.responseText)); break;
                    case 201: //Created
                        success(JSON.parse(http.responseText)); break;
                    case 204: //NoContent
                        success('ok'); break;
                    case 400: //BadRequest
                        failure('bad request'); break;
                    case 404: //NotFound
                        failure('not found'); break;
                    case 500: //ServerFailure
                        failure('server failure'); break;
                    default:
                        failure('invalid state: ' + http);
                }
            }
        };

        http.send(payload ? JSON.stringify(payload) : null);
    });

    return promise;
}


function addActions(buttons) {
    const items = document.querySelector('#items');
    const what = document.querySelector('#what');
    const when = document.querySelector('#when');
    const todo = (ev) => ev.target.parentNode.parentNode;

    for (let k = 0; k < buttons.length; ++k) {
        let b = buttons[k];
        switch (b.innerText) {
            case 'Complete':
                b.addEventListener('click', (ev) => {
                    const id = todo(ev).getAttribute(ID_KEY) || 0;
                    invokeBackend(BASE_URL + id, 'patch', { done: true })
                        .then(_ => {
                            let done = document.createElement('span');
                            done.classList.add('done');

                            let when = todo(ev).querySelector('.when');
                            when.querySelector('.date').classList.add('completed');
                            when.appendChild(done);

                            ev.target.setAttribute('disabled', true);
                        })
                        .catch(msg => alert(msg));
                });
                break;
            case 'Edit':
                b.addEventListener('click', (ev) => {
                    what.value = todo(ev).querySelector('.what').textContent.trim();
                    when.value = todo(ev).querySelector('.when').textContent.trim();
                    formTodoId = todo(ev).getAttribute(ID_KEY);
                });
                break;
            case 'Remove':
                b.addEventListener('click', (ev) => {
                    const id = todo(ev).getAttribute(ID_KEY) || 0;
                    invokeBackend(BASE_URL + id, 'delete')
                        .then(_ => {
                            items.removeChild(todo(ev));
                        })
                        .catch(msg => alert(msg));
                });
                break;
        }
    }
}

function createTodoItem(id, when, what, done = false) {
    const overdue = new Date(when).getTime() < new Date().getTime();
    let html = `
        <div class="float-right">
            <button type="button" class="btn btn-success btn-sm ml-1" ${done ? 'disabled' : ''}>Complete</button>
            <button type="button" class="btn btn-primary btn-sm">Edit</button>
            <button type="button" class="btn btn-danger btn-sm ml-1">Remove</button>
        </div>
        <p class="when ${overdue && !done ? 'overdue' : ''}"><span class="date">${when}</span>${done ? '<span class="done"></span>' : ''}</p>
        <p class="what">${what}</p>
    `;

    let todo = document.createElement('li');
    todo.setAttribute(ID_KEY, id);
    todo.className = 'list-group-item';
    todo.innerHTML = html;
    addActions(todo.querySelectorAll('button'));

    return todo;
}

function appendTodo(root, id, when, what, done = false) {
    root.appendChild(createTodoItem(id, when, what, done));
}

function loadAll() {
    const items = document.querySelector('#items');
    invokeBackend(BASE_URL, 'get')
        .then(objs => {
            items.innerHTML = '';
            objs.forEach(obj => appendTodo(items, obj.id, obj.when, obj.what, obj.done));
        })
        .catch(err => {
            alert(err);
        });
}

function clear(ev) {
    if (ev) ev.preventDefault();
    document.querySelector('#what').value = '';
    document.querySelector('#when').value = new Date().toLocaleDateString();
}

function save(ev) {
    ev.preventDefault();
    const items = document.querySelector('#items');
    const what = document.querySelector('#what').value;
    const when = document.querySelector('#when').value;

    if (formTodoId) {
        invokeBackend(BASE_URL + formTodoId, 'put', { when, what })
            .then(obj => {
                for (let k = 0; k < items.children.length; ++k) {
                    const item = items.children[k];
                    if (item.getAttribute(ID_KEY) == formTodoId) {
                        const newChild = createTodoItem(obj.id, obj.when, obj.what, obj.done);
                        items.replaceChild(newChild, item);
                        formTodoId = undefined;
                        clear();
                    }
                }
            })
            .catch(msg => alert(msg));
    } else {
        invokeBackend(BASE_URL, 'post', { when, what, done: false })
            .then(obj => {
                appendTodo(items, obj.id, obj.when, obj.what, obj.done);
                clear();
            })
            .catch(msg => alert(msg));
    }
}

function clearPhrase() {
    const phrase = document.querySelector('#phrase');
    phrase.value = '';
    loadAll();
}

function interactiveSearch(ev) {
    let phrase = document.querySelector('#phrase').value;
    if (phrase && phrase.trim().length > 0) {
        phrase = encodeURI(phrase.trim().toLowerCase());
        invokeBackend(BASE_URL + `?q=${phrase}`, 'get')
            .then(objs => {
                items.innerHTML = '';
                objs.forEach(obj => appendTodo(items, obj.id, obj.when, obj.what, obj.done));
            })
            .catch(msg => alert(msg));
    } else {
        clearPhrase();
    }
}


window.addEventListener('load', () => {
    document.querySelector('#clear').addEventListener('click', clear);
    document.querySelector('#save').addEventListener('click', save);
    document.querySelector('#phrase-clear').addEventListener('click', clearPhrase);
    document.querySelector('#phrase').addEventListener('keyup', interactiveSearch);
    clear();
    loadAll();
});

