const path = require('path');
const io   = require('./async-io');

const filename = process.argv[2] || process.argv[1];
const filename2 = 'xx-' + path.basename(filename);

io.readByPromise(filename)
    .then(contents => {
        console.log('read', contents.length, 'bytes from', filename);
        const payload = contents.toUpperCase().replace(/\s+/g, ':');
        return io.writeByPromise(filename2, payload);
    })
    .then(_ => {
        console.log('written file', filename2);
    })
    .catch(err => console.error('FAILURE:', err.message))
    ;
