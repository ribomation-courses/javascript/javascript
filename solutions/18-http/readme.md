# JSON Server

## Preparation
Download the NPM packages.

    npm install

## Launch Server
Start the server in a terminal.

    npm run server

## Investigate
Open a browser to [http://localhost:3000/](http://localhost:3000/) and look around.

Try some HTTP `GET` operations. If you have a HTTP client,
such as [HTTPie](https://httpie.org/doc#installation), 
[Postman](https://www.getpostman.com/downloads/)
 or similar,
then try out other operations, like `POST`, `PUT`, `DELETE`.


